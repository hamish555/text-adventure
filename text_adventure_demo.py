#game
import os

x, z = 0, 0
p1 = '@'
p2 = '@' 
p3 = '@'
p4 = '@'
p5 = '@'

def clear():
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')
def printBoard():
    clear()
    num = -1
    print('X - You\n. - CLear Tile\n-|/\\ - Walls\n+ - Door\n@ - Unsolved\n')
    print(x,'',z)
    for i in range(5):
        num += 5
        print(tile_bag[num-4],tile_bag[num-3],tile_bag[num-2],tile_bag[num-1],tile_bag[num])
def north(movement):
    global z
    if movement[0] == 'N':
        z += 1
        clear()
def east(movement):
    global x
    if movement[0] =='E':
        x += 1
        clear()
def south(movement):
    global z
    if movement[0] == 'S':
        z -= 1
        clear()
def west(movement):
    global x
    if movement[0] == 'W':
        x -= 1
        clear()
def choice():
    global movement
    movement = input('\nInput: ')
    movement = movement.upper()

clear()
try:
    while True:
        try:
            if x == 0 and z == 0:
                tile_bag = [' ',' ',' ',' ',' ',' ','-','+','-',' ',' ','|','X','|',' ',' ','-','-','-',' ',' ',' ',' ',' ',' ',' ']
                printBoard()
                print('\nChoices: North')
                choice()
                north(movement)
            elif x == 0 and z == 1:
                if p1 == '@':
                    tile_bag = [' ',' ',' ',' ',' ',' ','|',p1,'|',' ',' ','|','X','|',' ',' ','-','+','-',' ',' ',' ',' ',' ',' ',' ']
                else:
                    tile_bag = [' ','|','.','|',' ',' ','|',p1,'|',' ',' ','|','X','|',' ',' ','-','+','-',' ',' ',' ',' ',' ',' ',' ']
                printBoard()
                print('\nChoices: North, South')
                choice()
                north(movement)
                south(movement)
            elif x == 0 and z == 2:
                if p1 == '@':
                    tile_bag = [' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','|','X','|',' ',' ','|','.','|',' ',' ','-','+','-',' ',' ']
                else:
                    tile_bag = [' ','+','.','+',' ',' ','|','.','|',' ',' ','|','X','|',' ',' ','|','.','|',' ',' ','-','+','-',' ',' ']
                printBoard()
                if p1 == '@':
                    print('\nPuzzle: Tiles With The Symbol \'@\' Are Unsolved Puzzles\nSolve Puzzles And They Become Normal Tiles\nYour First Puzzle Is: What Is Your Name?')
                    name = input('\nInput: ')
                    if name:
                        p1 = "."
                else:
                    print('\nChoices: North, South')
                    choice()
                    north(movement)
                    south(movement)
            elif x == 0 and z == 3:
                tile_bag = [' ','|','.','|',' ',' ','+','.','+',' ',' ','|','X','|',' ',' ','|','.','|',' ',' ','|','.','|',' ',' ']
                printBoard()
                print('\nChoices: North, South')
                choice()
                north(movement)
                south(movement)
            elif x == 0 and z == 4:
                tile_bag = ['-','-','.','|',' ',' ','|','.','|',' ',' ','+','X','+',' ',' ','|','.','|',' ',' ','|','.','|',' ',' ']
                printBoard()
                print('\nChoices: North, East, South, West')
                choice()
                north(movement)
                east(movement)
                south(movement)
                west(movement)
            elif x == 0 and z == 5:
                tile_bag = ['.','.','.','|',' ','-','-','.','|',' ',' ','|','X','|',' ',' ','+','.','+',' ',' ','|','.','|',' ',' ']
                printBoard()
                print('\nChoices: North, South')
                choice()
                north(movement)
                south(movement)
            elif x == 0 and z == 6:
                tile_bag = ['-','-','-','-',' ','.','.','.','|',' ','-','-','X','|',' ',' ','|','.','|',' ',' ','+','.','+',' ',' ']
                printBoard()
                print('\nChoices: North, South')
                choice()
                north(movement)
                south(movement)
            elif x == 0 and z == 7:
                tile_bag = [' ',' ',' ',' ',' ','-','-','-','-',' ','.','.','X','|',' ','-','-','.','|',' ',' ','|','.','|',' ',' ']
                printBoard()
                print('\nChoices: South, West')
                choice()
                south(movement)
                west(movement)
            elif x == -1 and z == 7:
                tile_bag = [' ',' ',' ',' ',' ','-','-','-','-','-',p5,'.','X','.','|','-','-','-','.','|',' ',' ','|','.','|',' ']
                printBoard()
                print('\nChoices: East, West')
                choice()
                east(movement)
                west(movement)
            elif x == -2 and z == 7:
                tile_bag = [' ',' ',' ',' ',' ',' ','-','-','-','-',' ',p5,'X','.','.',' ','-','-','-','.',' ',' ',' ','|','.','|']
                printBoard()
                print('\nChoices: East, West')
                choice()
                east(movement)
                west(movement)
            elif x == -3 and z == 7:
                tile_bag = [' ',' ',' ',' ',' ',' ',' ','-','-','-',' ',' ','X','.','.',' ',' ','-','-','-',' ',' ',' ',' ','|']
                printBoard()
                if p1 != '@':
                    if p2 != '@':
                        if p3 != '@':
                            if p4 != '@':
                                print('\nWell Done You Finished The Demo, Leave Feedback If I Should Continue')
                                input('Press \'Enter\' To End')
                                clear()
                                quit()
                            else:
                                print('\nComplete The Rest Of The Puzzles')
                        else:
                            print('\nComplete The Rest Of The Puzzles')
                    else:
                        print('\nComplete The Rest Of The Puzzles')
                else:
                    print('\nComplete The Rest Of The Puzzles')
                print('\nChoices: East')
                choice()
                east(movement)
            elif x == -1 and z == 4:
                tile_bag = [' ','-','-','-',' ',' ','|',p2,'|',' ',' ','|','X','+',' ',' ','-','-','|',' ',' ',' ',' ',' ',' ',' ']
                printBoard()
                print('\nChoices: North, East')
                choice()
                north(movement)
                east(movement)
            elif x == -1 and z == 5:
                tile_bag = [' ',' ',' ',' ',' ',' ','-','-','-',' ',' ','|','X','|',' ',' ','|','.','+',' ',' ','-','-','|',' ',' ']
                printBoard()
                if p2 == '@':
                    print('\nPuzzle: What is 2+2')
                print('\nChoices: South')
                choice()
                if movement == '4':
                    p2 = '.'
                south(movement)
            elif x == 1 and z == 4:
                tile_bag = [' ','|','-','-',' ',' ','|',p4,'|',' ',' ','+','X','|',' ',' ','|',p3,'|',' ',' ','|','-','-',' ',' ']
                printBoard()
                print('\nChoices: North, South, West')
                choice()
                north(movement)
                south(movement)
                west(movement)
            elif x == 1 and z == 5:
                tile_bag = [' ',' ',' ',' ',' ',' ','|','-','-',' ',' ','|','X','|',' ',' ','+','.','|',' ',' ','|',p3,'|',' ',' ']
                printBoard()
                if p4 == '@':
                    print('\nPuzzle: What Is The First Letter Of The Alphabet')
                print('\nChoices: South')
                choice()
                if movement == 'A':
                    p4 = '.'
                south(movement)
            elif x == 1 and z == 3:
                tile_bag = [' ','|',p4,'|',' ',' ','+','.','|',' ',' ','|','X','|',' ',' ','|','-','-',' ',' ',' ',' ',' ',' ',' ']
                printBoard()
                if p3 == '@':
                    print('\nPuzzle: 5, 10, 15... What Is The Next Number')
                print('\nChoices: North')
                choice()
                if movement == '20':
                    p3 = '.'
                north(movement)
            else:
                clear()
                print('You Left The Map, No More Game')
                exit()
        except IndexError:
            pass
except EOFError:
    clear()
